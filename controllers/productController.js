
const Product = require('../Product')



const allProducts = async (req, res) => {
  try {
    const products = await Product.find()
    res.json(products)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}
const deleteProduct = async (req, res) => {
  try {
    const productId = req.params.id

    const result = await Product.deleteOne({ _id: productId })

    if (result.deletedCount === 0) {
      return res.status(404).json({ message: 'Product not found.' })
    }

    res.json({ message: 'Product deleted successfully.' })
  } catch (err) {
    res.status(400).json({ error: err.message })
  }
}
const updateProduct = async (req, res) => {
  try {
    const productId = req.params.id
    const updates = req.body

    const result = await Product.updateOne({ _id: productId }, updates)
    
    if (result.nModified === 0) {
      return res.status(404).json({ message: 'Product not found or no changes were made.' })
    }

    res.json({ message: 'Product updated successfully.' })
  } catch (err) {
    res.status(400).json({ error: err.message })
  }
};

module.exports = {
  allProducts,
  updateProduct,
  deleteProduct,
}




