const router = require("express").Router()
const {
  allProducts,
  updateProduct,
  deleteProduct
} = require("../controllers/productController")

router.get('/products', allProducts)
router.delete('/products/:id', deleteProduct)
router.put('/products/:id', updateProduct)

module.exports = router;